FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

ENV KAFKA_BROKER_HOST="134.60.152.152"
ENV KAFKA_BROKER_PORT=9092
ENV KAFKA_BROKER_TOPIC="hackathon2"
ENV INFLUX_DB_HOST="134.60.152.152"
ENV INFLUX_DB_PORT=8086
ENV INFLUX_DB_NAME="test"
ENV OPC_UA_HOST="169.254.255.86"
ENV OPC_UA_PORT=4840

CMD [ "python", "./main.py" ]