from influxdb import InfluxDBClient

#client = InfluxDBClient(host=influx_ip_address, port=influx_port, database="test")
#client.create_database('test')
#client.write_points(json_body)
#result = client.query('select value from test_measure_collections;')
#print("Result: {0}".format(result))


class InfluxProxy(object):

    def __init__(self, host, port, db, user, password):
        self.client = InfluxDBClient(host=host, port=port, database=db, username=user, password=password)
        self.client.create_database(db)

    def write(self, json_body):
        self.client.write_points(json_body)

    def query(self, q):
        return self.client.query(q)

    def stop(self):
        self.client.close()
        self.client=None
