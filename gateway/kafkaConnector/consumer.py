import json
import threading
from kafka import KafkaConsumer
from gateway.base import Message


class Consumer(threading.Thread):
    def __init__(self, host, port, topics):
        threading.Thread.__init__(self)
        self.stop_event = threading.Event()

        self.host = host
        self.port = port
        self.topics = topics

        self.gateway = None

    def set_parent(self, gateway):
        self.gateway = gateway

    def stop(self):
        self.stop_event.set()

    def run(self):
        consumer = KafkaConsumer(bootstrap_servers='{}:{}'.format(self.host, self.port),
                                 auto_offset_reset='latest',
                                 consumer_timeout_ms=1000)
        consumer.subscribe(self.topics)

        while not self.stop_event.is_set():
            for record in consumer:
                self.process(record)
                if self.stop_event.is_set():
                    break

        consumer.close()

    def process(self, record):
        print(record)

        msg = None
        try:
            msg = Message.deserialize(value=record.value.decode("utf-8"))
        except Exception as ex:
            print(ex)

        if msg is not None:
            if self.gateway is not None:
                self.gateway.process(msg)
            else:
                print(msg)
