import time
import threading

from kafka import KafkaProducer


class Producer(threading.Thread):
    def __init__(self, host, port, topic):
        threading.Thread.__init__(self)

        self.host = host
        self.port = port
        self.topic = topic

        self.stop_event = threading.Event()
        self.producer = KafkaProducer(bootstrap_servers='{}:{}'.format(self.host, self.port))

    def stop(self):
        self.stop_event.set()
        self.producer.close()

    def send(self, message):
        if not self.stop_event.is_set():
            print("Kafka-Producer: {} - {}".format(self.topic, message))
            print("Message: {}".format(message.serialize()))
            self.producer.send(self.topic, message.serialize().encode('utf-8'))

    def run(self):
        while not self.stop_event.is_set():
            self.producer.send(self.topic, b"Hallo")
            time.sleep(1)
        self.producer.close()
