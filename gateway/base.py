import json


class Message(object):
    def __init__(self, ts=None, station=None, variable=None, value=None):
        self.ts = ts
        self.station = station
        self.variable = variable
        self.value = value

    def __str__(self):
        return "Message(timestamp='{}', station='{}', variable='{}, value='{}')".format(self.ts, self.station,
                                                                                        self.variable, self.value)

    def serialize(self):
        return json.dumps({"ts": self.ts, "station": self.station, "variable": self.variable, "value": self.value})

    @classmethod
    def deserialize(cls, value=None):
        d = json.loads(value)
        return cls(**d)
