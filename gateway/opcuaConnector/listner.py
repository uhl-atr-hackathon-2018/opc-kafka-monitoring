import os
import datetime
import threading
import opcua
from gateway.base import Message


class OPCListener(object):
    def __init__(self, host, port=4840, producer=None):

        self.client = opcua.Client("opc.tcp://{}:{}/".format(host, port))
        self.producer = producer

        self.stop_event = threading.Event()
        self.handles = []

        self.client.connect()
        self.client.load_type_definitions()
        root = self.client.get_root_node()

        monitoringRootNode = root.get_child(
            ["0:Objects", "2:DeviceSet", "4:ECC2250 0.8S 1131", "4:Resources", "4:Application", "3:GlobalVars"])
        self.sub = self.client.create_subscription(500, self)
        # handle = self.sub.subscribe_data_change(monitoringRootNode)

        for station in monitoringRootNode.get_children():
            for variable in station.get_children():
                try:
                    handle = self.sub.subscribe_data_change(variable)
                    self.handles.push(handle)
                except:
                    continue

    def stop(self):
        for handle in self.handles:
            self.sub.unsubscribe(handle)
        self.sub.delete()
        self.client.disconnect()
        self.stop_event.set()

    def datachange_notification(self, node, val, data):
        tagname = node.nodeid.Identifier
        tagnameParts = tagname.split(".")
        stationName = tagnameParts[-2]
        variableName = tagnameParts[-1]
        variableValue = val

        # print("datachange_notification", stationName, variableName, variableValue)
        message = Message(ts=datetime.datetime.utcnow().timestamp(), station=stationName, variable=variableName,
                          value=variableValue)

        if self.producer is not None:
            self.producer.send(message)
        else:
            print("OPCUA-Connector: {}".format(message))
