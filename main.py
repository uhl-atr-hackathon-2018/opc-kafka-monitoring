import os
import datetime
import logging

kafka_ip_address = os.getenv("KAFKA_BROKER_HOST", "134.60.152.152")
kafka_port = int(os.getenv("KAFKA_BROKER_PORT", 9092))
kafka_topic = os.getenv("KAFKA_BROKER_TOPIC", "hackathon2")

influx_ip_address = os.getenv("INFLUX_DB_HOST", "134.60.152.152")
influx_port = int(os.getenv("INFLUX_DB_PORT", 8086))
influx_db = os.getenv("INFLUX_DB_NAME", "test")
influx_user = os.getenv("INFLUX_DB_USER", "admin")
influx_pass = os.getenv("INFLUX_DB_PASS", "")

opc_ua_ip_address=os.getenv("OPC_UA_HOST", "169.254.255.86")
opc_ua_port=int(os.getenv("OPC_UA_PORT", 4840))

from gateway.kafkaConnector.consumer import Consumer
from gateway.influxConnector.client import InfluxProxy

logging.basicConfig(
    format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
    level=logging.WARNING
)

class KafkaInfluxGateway(object):

    def __init__(self, kafkaConsumer, influx):
        self.kafkaConsumer = kafkaConsumer
        self.kafkaConsumer.set_parent(self)
        self.influx = influx

    def start(self):
        self.kafkaConsumer.start()

    def stop(self):
        self.kafkaConsumer.stop()
        self.influx.stop()

    def process(self, message):
        """
        TODO: processing model
        """
        try:
            print("Gateway: {}".format(message))
            entry=self.create_entry(message)
            self.influx.write(entry)
        except Exception as ex:
            print(ex)

    def create_entry(self, msg):
        json_body = [
            {
                "measurement": "{}_{}".format(msg.station,msg.variable),
                "tags": {
                    "StationName": msg.station,
                    "VarName": msg.variable,
                },
                "time": datetime.datetime.fromtimestamp(int(msg.ts)),
                "fields": {
                    "value": msg.value,
                }
            }
        ]
        return json_body


def run_influx_connector():
    influx = InfluxProxy(influx_ip_address, influx_port, influx_db, influx_user, influx_pass)
    consumer = consumer = Consumer(kafka_ip_address, kafka_port, [kafka_topic, ])
    gateway = KafkaInfluxGateway(consumer, influx)
    gateway.start()
    input("Press any key to exit...")
    gateway.stop()


def test_influx():
    json_body = [
        {
            "measurement": "FUR_state",
            "tags": {
                "StationName": "FUR",
                "VarName": "state",
            },
            "time": "2009-11-10T23:00:00Z",
            "fields": {
                "value": 42,
            }
        }
    ]
    print("Running influxConnector db test ...")
    influx = InfluxProxy(influx_ip_address, influx_port, influx_db)
    influx.write(json_body)
    result = influx.query('select value from FUR_state;')
    print("Result: {0}".format(result))
    print("Influx db test finished.")


def test_kafka_consumer():
    print("Running kafka consumer ...")
    consumer = Consumer(kafka_ip_address, kafka_port, [kafka_topic, ])
    consumer.start()
    input("Press any key...")
    consumer.stop()
    print("Kafka consumer stopped.")


def test_opcua_connector():
    print("Running opc ua connector test...")

    from gateway.opcuaConnector.listner import OPCListener
    opc=OPCListener(opc_ua_ip_address, opc_ua_port)
    input("Press any key to exit...")
    opc.stop()
    print("Opc UA connector test stopped.")

def run_opcua_connector():
    print("Running opc ua connector...")

    from gateway.kafkaConnector.producer import Producer
    producer=Producer(kafka_ip_address, kafka_port, kafka_topic)

    from gateway.opcuaConnector.listner import OPCListener
    opc=OPCListener(opc_ua_ip_address, opc_ua_port, producer)
    input("Press any key to exit...")
    opc.stop()
    print("Opc UA connector stopped.")


if __name__ == "__main__":

    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("--influx", action="store_true", dest="influx", default=False,
                      help="Run influxConnector process")
    parser.add_option("--testinflux", action="store_true", dest="testinflux", default=False,
                      help="Run influxConnector test process")
    parser.add_option("--testconsumer", action="store_true", dest="testconsumer", default=False,
                      help="Run kafka consumer test process")
    parser.add_option("--opcua", action="store_true", dest="opcua", default=False,
                      help="Run opc process")
    parser.add_option("--testopcua", action="store_true", dest="testopcua", default=False,
                      help="Run opc process")

    parser.parse_args()
    (options, args) = parser.parse_args()

    if options.influx:
        run_influx_connector()
    if options.testinflux:
        test_influx()
    if options.testconsumer:
        test_kafka_consumer()

    if options.opcua:
        run_opcua_connector()
    if options.testopcua:
        test_opcua_connector()

